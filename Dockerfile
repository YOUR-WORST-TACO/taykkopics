FROM python:3
ADD main.py /
ADD index.html /
ADD password.txt /
ADD worddict.txt /
RUN mkdir /pictures
CMD [ "python", "./main.py" ]
