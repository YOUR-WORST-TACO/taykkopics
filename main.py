import socketserver
import configparser
import mimetypes
import cgi
import logging
import os
from os import curdir
from random import randint
from http.server import BaseHTTPRequestHandler
import http


def human_to_byte(readable_size):
    units = {"B": 1, "K": 10 ** 3, "M": 10 ** 6, "G": 10 ** 9, "T": 10 ** 12}

    readable_size = readable_size.replace(" ", "")
    for i in range(len(readable_size)):
        char = readable_size[i].upper()
        if not char.isdigit() and char != '.' and i > 0:
            if char in units:
                return int(float(readable_size[:i])*units[char])
            else:
                return -1


class TaykkoThreader(socketserver.ThreadingMixIn, http.server.HTTPServer):
    pass

class TaykkoHandler(BaseHTTPRequestHandler):
    def do_POST(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        form = cgi.FieldStorage(
            fp=self.rfile,
            headers=self.headers,
            environ={'REQUEST_METHOD':'POST',
                     'CONTENT_TYPE':self.headers['Content-Type'],
            }
        )

        file = None
        file_ext = None
        new_file = None
        filename = None
        password = None
        protocol = CONFIG['DEFAULT']['Protocol']
        realpassword = CONFIG['DEFAULT']['Password'] != '' and CONFIG['DEFAULT']['Password'] or None

        maxFileSize=human_to_byte(CONFIG['DEFAULT']['MaxFileSize'])
        if maxFileSize < 0:
            maxFileSize = 50000000

        if int(self.headers['content-length']) > maxFileSize:
            logging.warning("File was too big to upload")
            return 1

        for item in form.list:
            if item.name == "file":
                file = item.file
                filename = item.filename
            elif item.name == "password":
                password = item.file

        if realpassword:
            if not password:
                logging.warning("No password provided")
                return 1

            if realpassword != password.read():
                logging.warning("Passwords do not match")
                return 1

        if filename:
            filename, file_ext = os.path.splitext(filename)

        with open('./worddict.txt', 'r+') as dictionary:
            dictionary_words = [line.rstrip() for line in dictionary.readlines()]

        original_word = False

        while not original_word:
            new_file = dictionary_words[randint(0, len(dictionary_words) - 1)] + \
                       dictionary_words[randint(0, len(dictionary_words) - 1)] + \
                       file_ext
            if not os.path.isfile(curdir + '/uploads/' + new_file):
                original_word = True

        with open(curdir + '/uploads/' + new_file, "wb") as writefile:
            writefile.write(file.read())

        if protocol != "http" and protocol != "https":
            protocol="http"

        self.wfile.write((protocol + '://' + CONFIG['DEFAULT']['Host'] + '/' + new_file + '\n').encode())

    def do_GET(self):
        self.send_response(200)
        if os.path.isfile(curdir + "/uploads" + self.path):
            print(mimetypes.guess_type('file.abcd'))
            mimetype = mimetypes.guess_type(self.path)[0]
            if not mimetype:
                mimetype="application/octet-stream"

            self.send_header("content-type", mimetype)
            self.end_headers()
            with open(curdir + "/uploads" + self.path, "rb") as picfile:
                self.wfile.write(picfile.read())
        else:
            self.send_header("content-type", "text/html")
            self.end_headers()
            with open(curdir + "/index.html", "rb") as index:
                self.wfile.write(index.read())


def main():
    global CONFIG, types
    CONFIG = configparser.ConfigParser()
    CONFIG['DEFAULT'] = {'Protocol': 'http',
                         'Host': '127.0.0.1',
                         'Port': '8888',
                         'Password': '',
                         'MaxFileSize': '50M'}
    CONFIG.read('config.ini')

    PORT = CONFIG['DEFAULT']['Port']
    if PORT.isdigit():
        PORT = int(PORT)
    else:
        logging.warning("port %s, in config is not integer."% PORT)
        PORT = 8888

    with TaykkoThreader(("", PORT), TaykkoHandler) as httpd:
        print( "Serving at Port", PORT)
        try:
            httpd.serve_forever()
        except KeyboardInterrupt:
            print('shutdown command received, shutting down')
            httpd.socket.close()


if __name__ == "__main__":
    main()
